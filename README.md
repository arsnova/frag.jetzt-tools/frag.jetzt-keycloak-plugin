# Create plugin

`mvn install` to build this project.

You can put the `frag-jetzt-keycloak-plugin.jar` inside the `providers` folder on the stndard keycloak dir.

The standard keycloak dir is often `/opt/keycloak`.