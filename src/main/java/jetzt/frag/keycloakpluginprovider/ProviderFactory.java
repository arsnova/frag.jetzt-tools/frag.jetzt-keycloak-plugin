package jetzt.frag.keycloakpluginprovider;

import org.keycloak.Config.Scope;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class ProviderFactory implements EventListenerProviderFactory {

  @Override
  public void close() {}

  @Override
  public EventListenerProvider create(KeycloakSession session) {
    return new ListenerProvider(session, "true".equals(System.getenv("FJ_TRACE")));
  }

  @Override
  public String getId() {
    return "frag_jetzt_keycloak_plugin";
  }

  @Override
  public void init(Scope scope) {
  }

  @Override
  public void postInit(KeycloakSessionFactory factory) {}
}
