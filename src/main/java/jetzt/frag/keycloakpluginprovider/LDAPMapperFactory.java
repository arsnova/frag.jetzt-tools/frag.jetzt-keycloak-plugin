package jetzt.frag.keycloakpluginprovider;

import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.storage.ldap.mappers.LDAPStorageMapperFactory;

public class LDAPMapperFactory
  implements LDAPStorageMapperFactory<FragjetztLDAPMapper> {

  @Override
  public FragjetztLDAPMapper create(
    KeycloakSession session,
    ComponentModel model
  ) {
    String prefix = System.getenv("FJ_LDAP_PREFIX");
    if (prefix == null || prefix.isEmpty()) {
      prefix = "fj-ldap-";
    }
    return new FragjetztLDAPMapper(
      session,
      model,
      "true".equals(System.getenv("FJ_TRACE")),
      prefix
    );
  }

  @Override
  public String getId() {
    return "fragjetzt-ldap-mapper";
  }
}
