package jetzt.frag.keycloakpluginprovider;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import jetzt.frag.keycloakpluginprovider.FragjetztEvent.FragjetztEventType;
import org.jboss.logging.Logger;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.events.admin.OperationType;
import org.keycloak.events.admin.ResourceType;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmProvider;

public class ListenerProvider implements EventListenerProvider {

  private static final ObjectMapper mapper = new ObjectMapper();
  private static final Logger logger = Logger.getLogger(ListenerProvider.class);

  private KeycloakSession session;
  private boolean trace;

  public ListenerProvider(KeycloakSession session, boolean trace) {
    this.session = session;
    this.trace = trace;
  }

  @Override
  public void close() {}

  @Override
  public void onEvent(Event event) {
    infoDump(event);
    String realmName = getRealmName(event.getRealmId());
    if (event.getType() == EventType.UPDATE_PROFILE) {
      onUpdate(realmName, event);
    } else if (event.getType() == EventType.DELETE_ACCOUNT) {
      onDelete(realmName, event);
    }
  }

  @Override
  public void onEvent(AdminEvent event, boolean b) {
    infoDump(event, b);
    if (!event.getResourceType().equals(ResourceType.USER)) {
      return;
    }
    boolean isUser =
      event.getResourcePath().startsWith("users/") &&
      event.getResourcePath().length() == 42;
    if (!isUser) {
      return;
    }
    final String userId = event.getResourcePath().substring(6);
    String realmName = getRealmName(event.getRealmId());
    if (event.getOperationType().equals(OperationType.DELETE)) {
      sendEvent(
        realmName,
        new FragjetztEvent(FragjetztEventType.DELETE_ACCOUNT, userId, null)
      );
    } else if (event.getOperationType().equals(OperationType.UPDATE)) {
      try {
        HashMap<?, ?> map = mapper.readValue(
          event.getRepresentation(),
          HashMap.class
        );
        String email = (String) map.get("email");
        HashMap<String, String> newMap = new HashMap<>(1);
        newMap.put("email", email);
        sendEvent(
          realmName,
          new FragjetztEvent(FragjetztEventType.UPDATE_ACCOUNT, userId, newMap)
        );
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private void onUpdate(String realmName, Event event) {
    final String email = event.getDetails().get("updated_email");
    if (email == null) {
      return;
    }
    final String userId = event.getUserId();
    HashMap<String, String> map = new HashMap<>(1);
    map.put("email", email);
    sendEvent(
      realmName,
      new FragjetztEvent(FragjetztEventType.UPDATE_ACCOUNT, userId, map)
    );
  }

  private void onDelete(String realmName, Event event) {
    final String customAction = event
      .getDetails()
      .get("custom_required_action");
    if (!"delete_account".equals(customAction)) {
      errLog("Custom action changed.");
    }
    final String userId = event.getUserId();
    sendEvent(
      realmName,
      new FragjetztEvent(FragjetztEventType.DELETE_ACCOUNT, userId, null)
    );
  }

  private void sendEvent(String realmName, FragjetztEvent event) {
    final String eventPassword = System.getenv(
      "FJ_EVENT_PASSWORD_" + realmName
    );
    if (eventPassword == null || eventPassword.isEmpty()) {
      errLog("FJ_EVENT_PASSWORD_" + realmName + ": No event password set!");
      return;
    }
    event.setEventPassword(eventPassword);
    final String strUrl = System.getenv("FJ_EVENT_URL_" + realmName);
    if (strUrl == null || strUrl.isEmpty()) {
      errLog("FJ_EVENT_URL_" + realmName + ": No event url set!");
      return;
    }
    try {
      URL url = new URL(strUrl);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("POST");
      con.setDoOutput(true);
      con.setRequestProperty("Content-Type", "application/json");
      con.setRequestProperty("Accept", "application/json");
      OutputStream os = con.getOutputStream();
      os.write(
        mapper.writeValueAsString(event).getBytes(StandardCharsets.UTF_8)
      );
      os.flush();
      os.close();
      int status = con.getResponseCode();
      if (status / 100 != 2) {
        errLog(
          "Error when sending Request: " +
          status +
          ": " +
          con.getResponseMessage()
        );
      }
      con.disconnect();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void infoDump(Event e) {
    if (!trace) {
      return;
    }
    StringBuilder builder = new StringBuilder();
    builder
      .append("[frag_jetzt_keycloak_plugin] Event")
      .append("\nTime: ")
      .append(e.getTime())
      .append("\ntype: ")
      .append(e.getType())
      .append("\nrealmId: ")
      .append(e.getRealmId())
      .append("\nclientId: ")
      .append(e.getClientId())
      .append("\nuserId: ")
      .append(e.getUserId())
      .append("\nsessionId: ")
      .append(e.getSessionId())
      .append("\nipAddress: ")
      .append(e.getIpAddress())
      .append("\nerror: ")
      .append(e.getError())
      .append("\ndetails: ")
      .append(e.getDetails());
    logger.info(builder.toString());
  }

  private void infoDump(AdminEvent e, boolean b) {
    if (!trace) {
      return;
    }
    StringBuilder builder = new StringBuilder();
    builder
      .append("[frag_jetzt_keycloak_plugin] AdminEvent - ")
      .append(b)
      .append("\nTime: ")
      .append(e.getTime())
      .append("\nrealmId: ")
      .append(e.getRealmId())
      .append("\nauthDetails: ")
      .append(e.getAuthDetails() != null)
      .append("\nresourceType: ")
      .append(e.getResourceType())
      .append("\noperationType: ")
      .append(e.getOperationType())
      .append("\nresourcePath: ")
      .append(e.getResourcePath())
      .append("\nrepresentation: ")
      .append(e.getRepresentation())
      .append("\nerror: ")
      .append(e.getError());
    logger.info(builder.toString());
  }

  private void errLog(String msg) {
    logger.error("[frag_jetzt_keycloak_plugin] " + msg);
  }

  private String getRealmName(String realmId) {
    RealmProvider provider = session.realms();
    String name = provider.getRealm(realmId).getName();
    provider.close();
    return name.toUpperCase().replace("-", "_");
  }
}
