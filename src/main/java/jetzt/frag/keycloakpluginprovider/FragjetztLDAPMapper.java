package jetzt.frag.keycloakpluginprovider;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.naming.AuthenticationException;
import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.ldap.LDAPStorageProvider;
import org.keycloak.storage.ldap.idm.model.LDAPObject;
import org.keycloak.storage.ldap.idm.query.internal.LDAPQuery;
import org.keycloak.storage.ldap.mappers.LDAPStorageMapper;
import org.keycloak.storage.user.SynchronizationResult;

public class FragjetztLDAPMapper implements LDAPStorageMapper {

  private static final Logger logger = Logger.getLogger(
    FragjetztLDAPMapper.class
  );

  private final KeycloakSession session;
  private final ComponentModel model;
  private final boolean trace;
  private final String prefix;

  public FragjetztLDAPMapper(
    KeycloakSession session,
    ComponentModel model,
    boolean trace,
    String prefix
  ) {
    this.session = session;
    this.model = model;
    this.trace = trace;
    this.prefix = prefix;
  }

  private static Set<String> getDnGroups(String prefix, String dn) {
    Set<String> groups = new HashSet<>();
    if (!dn.endsWith(",dc=fh-giessen-friedberg,dc=de")) {
      return groups;
    }
    int start = dn.indexOf(",ou=");
    int end = dn.indexOf(",", start + 1);
    while (start > 0 && end > 0) {
      String group = dn.substring(start + 4, end);
      if (!"People".equals(group)) {
        groups.add(prefix + group);
      }
      start = dn.indexOf(",ou=", end);
      end = dn.indexOf(",", start + 1);
    }
    return groups;
  }

  @Override
  public void close() {}

  @Override
  public SynchronizationResult syncDataFromFederationProviderToKeycloak(
    RealmModel realm
  ) {
    return new SynchronizationResult();
  }

  @Override
  public SynchronizationResult syncDataFromKeycloakToFederationProvider(
    RealmModel realm
  ) {
    return new SynchronizationResult();
  }

  @Override
  public List<UserModel> getGroupMembers(
    RealmModel realm,
    GroupModel group,
    int firstResult,
    int maxResults
  ) {
    return null;
  }

  @Override
  public void onImportUserFromLDAP(
    LDAPObject ldapUser,
    UserModel user,
    RealmModel realm,
    boolean isCreate
  ) {
    if (!isCreate) {
      return;
    }
    Set<String> toAdd = getDnGroups(prefix, ldapUser.getDn().toString());
    realm
      .getGroupsStream()
      .forEach(m -> {
        if (toAdd.remove(m.getName())) {
          user.joinGroup(m);
        }
      });
    for (String s : toAdd) {
      GroupModel m = realm.createGroup(s);
      user.joinGroup(m);
    }
  }

  @Override
  public void onRegisterUserToLDAP(
    LDAPObject ldapUser,
    UserModel localUser,
    RealmModel realm
  ) {}

  @Override
  public UserModel proxy(
    LDAPObject ldapUser,
    UserModel delegate,
    RealmModel realm
  ) {
    return delegate;
  }

  @Override
  public void beforeLDAPQuery(LDAPQuery query) {}

  @Override
  public boolean onAuthenticationFailure(
    LDAPObject ldapUser,
    UserModel user,
    AuthenticationException ldapException,
    RealmModel realm
  ) {
    return false;
  }

  @Override
  public List<UserModel> getRoleMembers(
    RealmModel realm,
    RoleModel role,
    int firstResult,
    int maxResults
  ) {
    return null;
  }

  @Override
  public Set<String> mandatoryAttributeNames() {
    return null;
  }

  @Override
  public LDAPStorageProvider getLdapProvider() {
    return null;
  }

  private void dumpUser(LDAPObject ldapUser, UserModel user) {
    System.out.println("uuid: " + ldapUser.getUuid());
    System.out.println("dn: " + ldapUser.getDn());
    System.out.println("objectclasses: " + ldapUser.getObjectClasses());
    System.out.println("Attributes: " + ldapUser.getAttributes());
    System.out.println("-----------------------------");
    System.out.println("id: " + user.getId());
    System.out.println("username: " + user.getUsername());
    System.out.println("created: " + user.getCreatedTimestamp());
    System.out.println("enabled: " + user.isEnabled());
    System.out.println("attrs: " + user.getAttributes());
    System.out.println("email: " + user.getEmail());
    System.out.println("verified: " + user.isEmailVerified());
  }
}
